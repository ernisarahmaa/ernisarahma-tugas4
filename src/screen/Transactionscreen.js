import React from 'react';
import {View, Text, ScrollView, Image, TouchableOpacity} from 'react-native';

const Transactionscreen = ({navigation, route}) => {
  return (
    <View style={{backgroundColor: '#F6F8FF', flex: 1}}>
      <TouchableOpacity
        onPress={() =>
          navigation.navigate('TransactionNavigation', {screen: 'Detail'})
        }>
        <View
          style={{
            marginTop: 9,
            backgroundColor: '#fff',
            paddingVertical: 25,
            marginHorizontal: 11,
            borderRadius: 8,
            paddingHorizontal: 10,
          }}>
          <View
            style={{
              flexDirection: 'row',
            }}>
            <Text style={{marginRight: 11, color: '#BDBDBD'}}>
              20 Desember 2020
            </Text>
            <Text style={{color: '#BDBDBD'}}>09:00</Text>
          </View>
          <View style={{marginVertical: 13}}>
            <Text style={{color: '#201F26', fontWeight: '500'}}>
              New Balance - Pink Abu - 40
            </Text>
            <Text style={{color: '#201F26'}}>Cuci Sepatu</Text>
          </View>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <Text style={{color: '#201F26'}}>
              Kode Reservasi :{' '}
              <Text style={{fontWeight: 'bold'}}>CS201201</Text>
            </Text>
            <Text
              style={{
                backgroundColor: '#fdefdb',
                color: '#ffc107',
                borderRadius: 10,
                width: 81,
                height: 21,
                textAlign: 'center',
              }}>
              Reserved
            </Text>
          </View>
        </View>
      </TouchableOpacity>
    </View>
  );
};

export default Transactionscreen;
