import React, {useState} from 'react';
import {
  View,
  Text,
  ScrollView,
  TextInput,
  Image,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import CheckBox from '@react-native-community/checkbox';

const Orderscreen = ({navigation, route}) => {
  const [isSelected, setSelection] = useState(false);
  return (
    <View style={{backgroundColor: '#F6F8FF', flex: 1}}>
      <ScrollView>
        <View
          style={{
            paddingHorizontal: 20,
            marginTop: 5,
            backgroundColor: '#fff',
          }}>
          <Text
            style={{
              marginTop: 25,
              color: '#BB2427',
              fontWeight: 'bold',
            }}>
            Merek
          </Text>
          <TextInput
            placeholder="Masukkan Merk Barang"
            style={{
              marginTop: 10,
              width: '100%',
              borderRadius: 8,
              backgroundColor: '#F6F8FF',
              paddingHorizontal: 10,
            }}
          />
          <Text
            style={{
              marginTop: 25,
              color: '#BB2427',
              fontWeight: 'bold',
            }}>
            Warna
          </Text>
          <TextInput
            placeholder="Warna Barang, cth : Merah - Putih "
            style={{
              marginTop: 10,
              width: '100%',
              borderRadius: 8,
              backgroundColor: '#F6F8FF',
              paddingHorizontal: 10,
            }}
          />
          <Text
            style={{
              marginTop: 25,
              color: '#BB2427',
              fontWeight: 'bold',
            }}>
            Ukuran
          </Text>
          <TextInput
            placeholder="Cth : S, M, L / 39,40"
            style={{
              marginTop: 10,
              width: '100%',
              borderRadius: 8,
              backgroundColor: '#F6F8FF',
              paddingHorizontal: 10,
            }}
          />
          <Text
            style={{
              marginTop: 25,
              color: '#BB2427',
              fontWeight: 'bold',
            }}>
            Photo
          </Text>
          <TouchableOpacity style={styles.menuItem}>
            <Image
              style={styles.menuIcon}
              source={require('../assets/icons/camera.png')}
            />
            <Text style={styles.menuText}>Add Photo</Text>
          </TouchableOpacity>
          <View style={styles.checkboxContainer}>
            <CheckBox
              value={isSelected}
              onValueChange={setSelection}
              style={styles.checkbox}
            />
            <Text style={styles.label}>Ganti Sol Sepatu</Text>
          </View>
          <View style={styles.checkboxContainer}>
            <CheckBox
              value={isSelected}
              onValueChange={setSelection}
              style={styles.checkbox}
            />
            <Text style={styles.label}>Jahit Sepatu</Text>
          </View>
          <View style={styles.checkboxContainer}>
            <CheckBox
              value={isSelected}
              onValueChange={setSelection}
              style={styles.checkbox}
            />
            <Text style={styles.label}>Repaint Sepatu</Text>
          </View>
          <View style={styles.checkboxContainer}>
            <CheckBox
              value={isSelected}
              onValueChange={setSelection}
              style={styles.checkbox}
            />
            <Text style={styles.label}>Cuci Sepatu</Text>
          </View>
          <Text
            style={{
              marginTop: 25,
              color: '#BB2427',
              fontWeight: 'bold',
            }}>
            Catatan
          </Text>
          <TextInput
            placeholder="Cth : ingin ganti sol baru"
            style={{
              marginTop: 10,
              width: '100%',
              height: 95,
              borderRadius: 8,
              backgroundColor: '#F6F8FF',
              paddingHorizontal: 10,
              textAlignVertical: 'top',
            }}
          />
          <TouchableOpacity
            style={{
              width: '100%',
              marginVertical: 40,
              backgroundColor: '#BB2427',
              borderRadius: 8,
              paddingVertical: 15,
              justifyContent: 'center',
              alignItems: 'center',
            }}
            onPress={() => navigation.navigate('Cart')}>
            <Text
              style={{
                color: '#fff',
                fontSize: 16,
                fontWeight: 'bold',
              }}>
              Masukkan Keranjang
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  menuItem: {
    width: 95,
    height: 95,
    borderRadius: 8,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 1,
    borderColor: '#BB2427',
    marginTop: 20,
    marginBottom: 40,
  },
  menuIcon: {
    width: 20,
    height: 20,
    resizeMode: 'contain',
  },
  menuText: {
    color: '#BB2427',
    fontSize: 15,
    fontWeight: '600',
    paddingTop: 5,
  },
  checkboxContainer: {
    flexDirection: 'row',
    marginBottom: 20,
  },
  checkbox: {
    alignSelf: 'center',
  },
  label: {
    margin: 8,
  },
});
export default Orderscreen;
