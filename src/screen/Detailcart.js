import React from 'react';
import {View, Text, ScrollView, Image, TouchableOpacity} from 'react-native';

const DetailCartscreen = ({navigation, route}) => {
  return (
    <View style={{backgroundColor: '#F6F8FF', flex: 1}}>
      <ScrollView>
        <View
          style={{
            marginBottom: 12,
            backgroundColor: '#fff',
            padding: 25,
            justifyContent: 'center',
            borderRadius: 8,
          }}>
          <Text style={{marginBottom: 10, fontWeight: '500'}}>
            Data Customer
          </Text>
          <View style={{flexDirection: 'row'}}>
            <Text style={{marginRight: 10, color: '#201F26'}}>Agil Bani</Text>
            <Text style={{color: '#201F26'}}>(0813763476)</Text>
          </View>
          <Text style={{color: '#201F26'}}>
            Jl. Perumnas, Condong catur, Sleman, Yogyakarta
          </Text>
          <Text style={{color: '#201F26'}}>gantengdoang@dipanggang.com</Text>
        </View>
        <View
          style={{
            marginBottom: 12,
            backgroundColor: '#fff',
            padding: 25,
            justifyContent: 'center',
            borderRadius: 8,
          }}>
          <Text style={{marginBottom: 10, fontWeight: '500'}}>
            Alamat Outlet Tujuan
          </Text>
          <View style={{flexDirection: 'row'}}>
            <Text style={{marginRight: 10, color: '#201F26'}}>
              Jack Repair - Seturan
            </Text>
            <Text style={{color: '#201F26'}}>(027-343457)</Text>
          </View>
          <Text style={{color: '#201F26'}}>
            Jl. Affandi No 18, Sleman, Yogyakarta
          </Text>
        </View>
        <View
          style={{
            marginBottom: 12,
            backgroundColor: '#fff',
            padding: 25,
            justifyContent: 'center',
            borderRadius: 8,
          }}>
          <Text style={{marginBottom: 10, fontWeight: '500'}}>Barang</Text>
          <View style={{flexDirection: 'row'}}>
            <Image
              style={{width: 85, height: 85}}
              source={require('../assets/images/shoes1.png')}
            />
            <View style={{justifyContent: 'space-evenly', marginLeft: 13}}>
              <Text style={{fontWeight: '500', color: '#000'}}>
                New Balance - Pink Abu - 40
              </Text>
              <Text>Cuci Sepatu</Text>
              <Text>Note : -</Text>
            </View>
          </View>
        </View>
      </ScrollView>
      <View
        style={{
          marginHorizontal: 20,
          marginBottom: 48,
        }}>
        <TouchableOpacity
          style={{
            borderRadius: 8,
            backgroundColor: '#BB2427',
            height: 55,
            justifyContent: 'center',
            alignItems: 'center',
          }}
          onPress={() => navigation.navigate('Reservasi')}>
          <Text style={{color: '#fff', fontSize: 16, fontWeight: 'bold'}}>
            Reservasi Sekarang
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default DetailCartscreen;
