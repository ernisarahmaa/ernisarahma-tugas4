import React from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  ScrollView,
  StyleSheet,
} from 'react-native';

const Reservasiscreen = ({navigation, route}) => {
  return (
    <View style={{flex: 1, backgroundColor: '#FFFFFF'}}>
      <TouchableOpacity
        onPress={() =>
          navigation.navigate('HomeNavigation', {screen: 'Detail Cart'})
        }>
        <View>
          <Image
            source={require('../assets/icons/close.png')}
            style={{
              tintColor: '#2E3A59',
              width: 24,
              height: 24,
              marginTop: 22,
              marginLeft: 16,
            }}
          />
        </View>
      </TouchableOpacity>
      <ScrollView
        contentContainerStyle={{flexGrow: 1, justifyContent: 'center'}}>
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text style={{color: '#11A84E', fontSize: 20, fontWeight: '700'}}>
            Reservasi Berhasil
          </Text>
          <Image
            source={require('../assets/icons/checklist.png')}
            style={{
              width: 230,
              height: 238,
              marginVertical: 50,
            }}
          />
          <Text style={{color: '#000', fontSize: 20, textAlign: 'center'}}>
            Kami Telah Mengirimkan Kode {'\n'} Reservasi ke Menu Transaksi
          </Text>
        </View>
      </ScrollView>
      <View
        style={{
          marginHorizontal: 20,
          marginBottom: 48,
        }}>
        <TouchableOpacity
          style={{
            borderRadius: 8,
            backgroundColor: '#BB2427',
            height: 55,
            justifyContent: 'center',
            alignItems: 'center',
          }}
          onPress={() => navigation.navigate('Transaction')}>
          <Text style={{color: '#fff', fontSize: 16, fontWeight: 'bold'}}>
            Lihat Kode Reservasi
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default Reservasiscreen;
